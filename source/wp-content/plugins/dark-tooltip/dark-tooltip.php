<?php
/*
Plugin Name: dark-tooltip
Plugin URI: http://rubentd.com/darktooltip/
Description: tooltip plugins base on jquery dark-tooltip
Author: Son Nguyen Minh
Version: 1.0
Author URI: http://congnghefibo.com/
*/

add_action('wp_footer','dark_tooltip_init');

function dark_tooltip_init()
{
	wp_enqueue_script( 'dark-tooltip-js', plugins_url( 'jquery.darktooltip.min.js?v=1.0',__FILE__) );
	wp_enqueue_style('dark-tooltip-css', plugins_url('darktooltip.min.css', __FILE__ ) );
}