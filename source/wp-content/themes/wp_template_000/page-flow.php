<?php get_header(); ?>
<div class="primary-row clearfix">	
	<h2 class="h2_title">お問い合わせからお引渡しまで</h2>	
	<h3 class="flow-title mt30">
		STEP1 お問い合わせ・ヒアリング
	</h3>
	<div class="flow-content">
		<div class="flow-left flow-225 clearfix">									
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="flow" />			
			</div>
			<div class="text ln2em">
				<p>まずはお気軽にお問い合わせください。</p>
				<p class="pt20">電話、メール、FAXにて受け付けております。</p>
				<p>メール、FAXの場合は、お問い合わせの確認後3日以内に、<br />
				当社より確認のお電話をいたします。</p>
			</div>							
		</div>	
		<div class="flow-contact">
			<div class="flow-content-tel">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_tel.jpg" alt="flow" />
			</div>
			<div class="flow-content-contact">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_con.jpg" alt="flow" />
				</a>
			</div>	
		</div>
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>	
	</div>
	
	<h3 class="flow-title">
		STEP2 現地調査
	</h3>
	<div class="flow-content">
		<div class="flow-left flow-225 clearfix">									
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="flow" />			
			</div>
			<div class="text ln2em">
				<p>直接お伺いし、施工個所を実際に拝見いたします。<br />
				もちろん、お客様のご都合に合わせていただきますので、お気軽にお申し付けください。</p>
			</div>							
		</div>			
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>	
	</div>
	
	<h3 class="flow-title">
		STEP3 お見積もり
	</h3>
	<div class="flow-content">
		<div class="flow-left flow-225 clearfix">									
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="flow" />			
			</div>
			<div class="text ln2em">
				<p>現場調査の結果から、詳細なお見積もりをお出しします。<br/>
				ご不明点などがございましたら、ご納得のいくまでお尋ねください。</p>
			</div>							
		</div>			
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>	
	</div>
	
	<h3 class="flow-title">
		STEP4 ご契約
	</h3>
	<div class="flow-content">
		<div class="flow-left flow-225 clearfix">									
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="flow" />			
			</div>
			<div class="text ln2em">
				<p>ご納得いただければ契約となります。</p>
			</div>							
		</div>			
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>	
	</div>
	
	<h3 class="flow-title">
		STEP5 お打ち合わせ
	</h3>
	<div class="flow-content">
		<div class="flow-left flow-225 clearfix">									
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="flow" />			
			</div>
			<div class="text ln2em">
				<p>工事に向けた詳細なお打ち合わせを行います。</p>
				<p>疑問に思われた点などがございましたら、どんなことでもお気軽にお聞きください。</p>
			</div>							
		</div>			
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>	
	</div>
	
	<h3 class="flow-title">
		STEP6 完成～お引き渡し
	</h3>
	<div class="flow-content">
		<div class="flow-left flow-225 clearfix">									
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.jpg" alt="flow" />			
			</div>
			<div class="text ln2em">
				<p>施工が完了いたしましたら、お客様ご自身でのご確認をお願いいたします。</p>
				<p>OKをいただき、お引渡となります。</p>
			</div>							
		</div>			
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>	
	</div>
	
	<h3 class="flow-title">
		STEP7 アフターフォローについて
	</h3>
	<div class="flow-content">
		<div class="flow-left flow-225 clearfix">									
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img7.jpg" alt="flow" />			
			</div>
			<div class="text ln2em">
				<p>何か問題がございましたら、ご連絡をお願いいたします。</p>
				<p>即時対応させていただいております。</p>
			</div>							
		</div>				
	</div>	
</div>

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>