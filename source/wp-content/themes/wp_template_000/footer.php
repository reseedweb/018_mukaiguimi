                                </main><!-- end primary -->
                                <aside class="sidebar"><! begin sidebar -->
                                    <?php get_sidebar(); ?>
                                </aside>                            
                            </div><!-- end two-cols -->
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->                    
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
                            <div class="footer-content clearfix">
                                <div class="logo">
                                    <a href="<?php bloginfo('url'); ?>/">
                                    <img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="logo" />
                                    </a>
                                </div>
                                <div class="tel">
                                    <img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="tel" />
                                </div>
                                <div class="con">
                                    <a href="<?php bloginfo('url'); ?>/contact">
                                    <img src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" alt="contact" />
                                    </a>
                                </div>                                                                
                            </div>
                            <div class="footer-line"></div>
                            <div class="footer-menu clearfix">
                                <ul class="pull-left">
                                    <li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-chevron-right"></i>ホーム</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/about"><i class="fa fa-chevron-right"></i>宅地造成とは</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/business"><i class="fa fa-chevron-right"></i>事業案内</a></li>
                                </ul>
                                <ul class="pull-left">
                                    <li><a href="<?php bloginfo('url'); ?>/reliable"><i class="fa fa-chevron-right"></i>３つの安心宣言</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/work"><i class="fa fa-chevron-right"></i>施工事例</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/voice"><i class="fa fa-chevron-right"></i>お客様の声</a></li>
                                </ul>                                
                                <ul class="pull-left">
                                    <li><a href="<?php bloginfo('url'); ?>/facility"><i class="fa fa-chevron-right"></i>設備紹介</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/flow"><i class="fa fa-chevron-right"></i>施行までの流れ</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/staff"><i class="fa fa-chevron-right"></i>従業員一同</a></li>                                
                                </ul>
                                <ul class="pull-left">
                                    <li><a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-chevron-right"></i>よくあるご質問</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/contact"><i class="fa fa-chevron-right"></i>お問い合わせ</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-chevron-right"></i>会社概要</a></li>
                                </ul>
                                <ul class="pull-left">
                                    <li><a href="<?php bloginfo('url'); ?>/privacy"><i class="fa fa-chevron-right"></i>プライバシポリシー</a></li>
                                </ul>
                            </div>
                            <div class="copyright text-center">
                                Copyright © 2014 株式会社向井組  All Rights Reserved.
                            </div>                            
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->            
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->       
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">

            jQuery(document).ready(function(){               
                jQuery('.bxslider').bxSlider({
                    /*pagerCustom: '#bx-pager',*/
                    pause : 3000,
                    auto : true,
                });
                jQuery('.darktooltip').darkTooltip({
                    theme : 'light'
                });                
            }); 
        </script>         
    </body>
</html>