<div class="headerbtn">
	<div class="container"><div class="row"><div class="col-md-18">
		<div class="headerbtn-content">
			<ul class="clearfix">
				<li><a class="darktooltip" data-tooltip="#header_btn1" href="<?php bloginfo('url'); ?>/about">
					<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn1.jpg" alt="about" />
				</a></li>
				<li><a class="darktooltip" data-tooltip="#header_btn2" href="<?php bloginfo('url'); ?>/business">
					<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn2.jpg" alt="business" />
				</a></li>
				<li><a class="darktooltip" data-tooltip="#header_btn3" href="<?php bloginfo('url'); ?>/business">
					<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn3.jpg" alt="business" />
				</a></li>
				<li><a class="darktooltip" data-tooltip="#header_btn4" href="<?php bloginfo('url'); ?>/business">
					<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn4.jpg" alt="business" />
				</a></li>
				<li><a class="darktooltip" data-tooltip="#header_btn5" href="<?php bloginfo('url'); ?>/business">
					<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn5.jpg" alt="business" />
				</a></li>								
			</ul>
		</div>
	</div></div></div>
	<div class="data-tooltip">
		<div id="header_btn1">
			遊んでいる土地や田畑など<br/>
			土地を宅地として使用できる<br/>
			ようにする工事です。			
		</div>
		<div id="header_btn2">
			古くなった擁壁や<br/>
			石積みを主に解体します。<br/>
			時には家屋からの<br/>
			解体作業もおこないます。		
		</div>		
		<div id="header_btn3">
		更地にした土地を舗装します。<br/>
		アスファルト舗装、コンクリート舗装、インターロッキング工事にも対応いたします。		
		</div>
		<div id="header_btn4">
			アスファルト舗装、車止め設置、ライン引き…。簡単な補修から、カラー舗装や再舗装まで<br/>
			おまかせください。
		</div>
		<div id="header_btn5">
			フェンス、擁壁工事、ブロック積みから、カーポート設置などのエクステリア施工まで、<br/>
			トータルで対応		
		</div>		
	</div>
</div>