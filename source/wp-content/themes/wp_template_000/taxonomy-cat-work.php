<?php get_header(); ?>
	<div class="primary-row clearfix">
    <p>
		向井組の施工例です。宅地造成・造成工事はもちろん、解体工事・舗装工事・駐車場工事まで、豊富な施工例で住まいをサポートします。
	</p>
	<div class="primary-row clearfix">
		<div id="work-detail-navi">
	        <ul class="work-detail-navi clearfix">
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/造成工事-宅地造成">造成工事・宅地造成</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/解体工事">解体工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/舗装工事">舗装工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/駐車場工事">駐車場工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/外構工事">外構工事</a></li>                                                
	        </ul>	
		</div>
	</div>
	
		<?php
			$queried_object = get_queried_object();
			$term_id = $queried_object->term_id;
			//print_r($queried_object);
			?>
			<?php $posts = get_posts(array(
				'post_type'=> str_replace('cat-','',$queried_object->taxonomy),
				'posts_per_page' => get_query_var('posts_per_page'),
				'paged' => get_query_var('paged'),

				'tax_query' => array(
					array(
					'taxonomy' => $queried_object->taxonomy,
					'field' => 'term_id',
					'terms' => $term_id))
			));

	    ?>
	    <div class="message-group clearfix">
	    <?php $i = 0; ?>
	    <?php foreach($posts as $p) :  ?>	  
			<?php $i++; ?>
	        <?php if($i%2 == 1) : ?>
	        <div class="message-row clearfix">
	        <?php endif; ?>			      
				<div class="message-col message-col370 archive-work">			
					<div class="about-title"> <a href="<?php the_permalink(); ?>"><?php echo $p->post_title; ?></a></div>
					 <div class="image">
					 <a href="<?php the_permalink(); ?>">
								<?php echo get_the_post_thumbnail( $p->ID,'small'); ?>                    			                    
							</a>
						
					</div>															
					<div class="pt10">
						<span class="work-category"><?php @the_terms($p->ID, 'cat-work'); ?></span>
						<span class="work-date"><?php echo get_the_date('Y/m/d', $p->ID); ?></span>
					</div>					        	            					
				</div>
				<?php if($i%2 == 0 || $i == count($posts) ) : ?>
	        </div>
	        <?php endif; ?>
	    <?php endforeach; ?>		
	    </div>
		<div class="primary-row voice-page">
	        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	    </div>
	    <?php wp_reset_query(); ?>    		
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>