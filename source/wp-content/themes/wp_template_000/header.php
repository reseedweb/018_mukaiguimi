<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />        
        <link rel="shortcut icon" href="" />
        
        <!-- global javascript variable -->
        <script type="text/javascript" language="javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        

        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->

            <section id="top"><!-- begin top -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
                            <div class="top-content clearfix"><!-- begin .top-content -->
                                <h1 class="top-text">大阪・和歌山・滋賀の宅地造成・造成工事なら向井組におまかせください</h1>
                                <ul class="top-list">
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/flow">
                                            <i class="fa fa-caret-right"></i>施工までの流れ
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/faq">
                                            <i class="fa fa-caret-right"></i>よくあるご質問
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/company">
                                            <i class="fa fa-caret-right"></i>会社概要
                                        </a>
                                    </li>       
                                </ul>
                            </div><!-- .top-content -->
                        </div><!-- end col -->                        
                    </div><!-- end row -->
                </div><!-- end container -->            
            </section><!-- end top -->            

            <header><!-- begin header -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->                            
                            <div class="header-content clearfix"><!-- begin header-content --> 
                                <div class="logo"><!-- begin logo -->
                                    <a href="<?php bloginfo('url'); ?>/">
                                        <img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" />
                                    </a>
                                </div><!-- end logo -->
                                <div class="header-con">
                                    <a href="<?php bloginfo('url'); ?>/contact">
                                        <img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" />
                                    </a>                                
                                </div><!-- end header-con -->
                                <div class="header-tel">
                                    <img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" />                                    
                                </div><!-- end header-con -->                                
                            </div><!-- end header-content -->                            
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->

                <?php get_template_part('part','nav'); ?>

            </header><!-- end header -->

            <?php if(is_front_page()): ?>
            <section id="feature">            
                <?php get_template_part('part','mainslide'); ?>
                <?php get_template_part('part','headerbtn'); ?>            
            </section>
            <?php else : ?>
            <section id="page-feature">
                <div class="container"><div class="row"><div class="col-md-18">
                    <?php if(is_archive('work')) : ?>
					<div class="page-feature-content">
                        <img src="<?php bloginfo('template_url'); ?>/img/common/page_feature_img.jpg" alt="page feature" />
                        <h3 class="title">施工事例</h3>
                    </div>
					<?php else : ?>
					<div class="page-feature-content">
                        <img src="<?php bloginfo('template_url'); ?>/img/common/page_feature_img.jpg" alt="page feature" />
                        <h3 class="title"><?php the_title(); ?></h3>
                    </div>
					 <?php endif; ?>   
                </div></div></div>
                <section id="breadcrumb">
					<?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </section>                
            </section>
            <?php endif; ?>

            <section id="content"><!-- begin content -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
                            <div class="content two-cols-left clearfix"><!-- begin two-cols --> 
                                <main class="primary"><!-- begin primary -->          
                