<nav class="globalNavi-static">
    <div class="container"><!-- begin container -->
        <div class="row clearfix"><!-- begin row -->
            <div class="col-md-18"><!-- begin col -->
            	<div class="globalNavi-content">					
            		<ul>
            			<li><a href="<?php bloginfo('url'); ?>/about">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img1.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/business">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img2.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/reliable">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img3.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/work">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img4.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/voice">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img5.jpg" alt="about" />
            			</a></li><!-- end li -->
                        <li><a href="<?php bloginfo('url'); ?>/facility">
                            <img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img6.jpg" alt="about" />
                        </a></li><!-- end li -->
                        <li><a href="<?php bloginfo('url'); ?>/staff">
                            <img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img7.jpg" alt="about" />
                        </a></li><!-- end li -->
                        <li><a href="<?php bloginfo('url'); ?>/contact">
                            <img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img8.jpg" alt="about" />
                        </a></li><!-- end li -->
            		</ul>
				</div>                
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->	
</nav>